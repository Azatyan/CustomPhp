<?php

class Login_Model extends Model
{
    protected $db;

    public function __construct()
    {
        parent::__construct();
        $this->db = new Database();
    }

    public function register()
    {
        $data = $_POST;
        $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
        unset($data['repeat_password']);


        $this->db->insert('users');
        $this->db->insertValues($data);
        $data =  $this->db->execute();

        $count = count($data);

        if ($count > 0){
            Session::init();
            Session::set('role', $data['role']);
            Session::set('loggedIn', true);
            header('location: /');
        }
        else{
            header('location: login');
        }
    }

    public function login()
    {
        $data = $_POST;
        $this->db->select('users');
        $user = $this->db->where('login', '=', $_POST['login']);
        $count = count($user);
        if ($count > 0){
            if (password_verify($data['password'], $user->password)) {
                Session::init();
                Session::set('role', $data['role']);
                Session::set('loggedIn', true);
                header('location: /');
            }
        }

    }

}