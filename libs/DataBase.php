<?php

class Database extends PDO
{
    public $db;
    public $query;
    public $values = [];

    public function __construct ()
    {
        //parent::__construct(DB_TYPE.':host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);
        $this->db = new PDO(DB_TYPE.':host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);
    }

   public function query($sql, $values = [])
    {
        $stmt = $this->db->prepare($sql);
        $stmt->execute($values);

        if($stmt->rowCount() === 1) return $stmt->fetch(PDO::FETCH_OBJ);
        if($stmt->rowCount() > 1) return $stmt->fetchAll(PDO::FETCH_OBJ);

        return false; // No results
    }

    public function update($table)
    {
        $this->query = "UPDATE {$table} ";

        return $this;
    }

    public function select($table)
    {
        $this->query = "SELECT * FROM {$table} ";

        return $this;
    }

    public function insert($table)
    {
        $this->query = "INSERT INTO {$table} (";

        return $this;
    }

    public function insertValues($columnValues = [])
    {
        $values = '(';
        foreach($columnValues as $column => $value)
        {
            $this->query .= "{$column}, ";
            $this->values[] = $value;
            $values .= "?,";
        }
        $this->query = rtrim($this->query,", ");
        $values = rtrim($values,", ");

        $this->query .= ') VALUES '.$values. ")";

        return $this;

    }

    public function with($columnValues = [])
    {
        $this->query .= "SET ";

        foreach($columnValues as $column => $value)
        {
            $this->query .= "{$column} = ?, ";
            $this->values[] = $value;
        }

        $this->query .= 'updated_at = NOW()';

        return $this;
    }

    public function where($column, $operator, $value)
    {
        $this->query .= " WHERE {$column} {$operator} ?";
        $this->values[] = $value;

        return $this->execute();
    }

    public function execute()
    {
        return $this->query($this->query, $this->values);
    }

    public function staticType($value)
    {
        $value = is_int($value) ? (int) $value : $value;
        $value = is_string($value) ? '"' . $value . '"' : $value;

        return $value;
    }


}