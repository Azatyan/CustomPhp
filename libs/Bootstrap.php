<?php

class Bootstrap {
    function __construct() {
        if(isset($_GET['url'])) {
            $url = rtrim($_GET['url'], '/');
            $url = explode('/', $url);
            $controllerName = $url[0]. 'Controller';

            $file = 'controllers/'. $controllerName . '.php';
            if(file_exists($file)) {
                require $file;
            }
            else {
                throw new Exception('The file: '.$file.'does not exists.');
            }

            $controller = new $controllerName;
            $controller->loadModel($url[0]);

            if(isset($url[2])) {
                $controller->$url[1]($url[2]);
            }
            else if(isset($url[1])) {
                $controller->$url[1]();
            }
            else{
                $controller->index();
            }
        } else{
            require 'controllers/indexController.php';
            $controller = new indexController();
            $controller->index();
            $controller->loadModel('index/index');
            return false;
        }
    }
}