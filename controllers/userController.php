<?php

class userController extends Controller {
    function __construct()
    {
        parent::__construct();
        Session::init();
        $logedIn = Session::get('loggedIn');
        $role = Session::get('role');

        if ($logedIn == false || $role != 'owner'){
            Session::destroy();
            header('location: login');
            exit;
        }

        $this->view->js = array();
    }

    public function index() {
        $this->view->render('user/index');
    }



}


