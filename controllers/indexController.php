<?php

class indexController extends Controller {
    function __construct() {
        parent::__construct();
        session_start();
    }

    public function index() {
        if (!Session::get('loggedIn')){
            header('location: /login');
        }
        $this->view->render('index/index');
    }
}


