<?php

class loginController extends Controller
{
    function __construct()
    {
        parent::__construct();
        $this->view->css = ['public/css/login_register.css'];
        $this->view->js = ['public/js/login_register.js'];
        session_start();
    }

    public function index()
    {
        if (Session::get('loggedIn')){
            header('location: /');
        }
        $this->view->render('login/index');
    }

    function login()
    {
        $this->model->login();
    }

    function logout()
    {
        session_start();
        session_destroy();
        unset($_SESSION);
        header('location: /login');
        exit;
    }

    public function register()
    {
        $this->model->register();
    }
}


