
<div id="login">
    <div class="container">
        <div class="card"></div>
        <div class="card">
            <h1 class="title">Login</h1>
            <form action="/login/login" method="POST">
                <div class="input-container">
                    <input type="text" id="login" name="login" required="required"/>
                    <label for="login">Username</label>
                    <div class="bar"></div>
                </div>
                <div class="input-container">
                    <input type="password" name="password" id="password" required="required"/>
                    <label for="password">Password</label>
                    <div class="bar"></div>
                </div>
                <div class="button-container">
                    <button><span>Go</span></button>
                </div>
            </form>
        </div>
        <div class="card alt">
            <div class="toggle"></div>
            <h1 class="title">Register
                <div class="close"></div>
            </h1>
            <form action="/login/register" method="post">
                <div class="input-container">
                    <input type="text" name="login" id="username" required="required"/>
                    <label for="username">Username</label>
                    <div class="bar"></div>
                </div>
                <div class="input-container">
                    <input type="password" name="password" id="pass" required="required"/>
                    <label for="pass">Password</label>
                    <div class="bar"></div>
                </div>
<!--                <div class="input-container">-->
<!--                    <input type="password" name="repeat_password" id="repeat_pass" required="required"/>-->
<!--                    <label for="repeat_pass">Repeat Password</label>-->
<!--                    <div class="bar"></div>-->
<!--                </div>-->
                <div class="button-container">
                    <button><span>Next</span></button>
                </div>
            </form>
        </div>
    </div>
</div>
