<!DOCTYPE html>
<html>
<head>
<title>Title of the document</title>
    <link rel="stylesheet" type="text/css" href="<?php echo URL;?>public/css/main.css">
    <script type="text/javascript" src="<?php echo URL;?>public\jquery-3.2.1.min.js"></script>
    <?php
        if(isset($this->css)){
            foreach ($this->css as $css) {
                echo  '<link rel="stylesheet" type="text/css" href="' . URL . $css . '">';
            }
        }
    ?>
</head>

<body>
    <div class="content">
        <?php session::init(); ?>
        <div id="header">
        <?php
            if(Session::get('loggedIn') == true):?>
                <a href="<?php echo URL;?>">Home</a>
                <a class="logout" href="<?php echo URL;?>login/logout">Logout</a>
                <?php if(Session::get('role') == 'owner'): ?>
                    <a href="<?php echo URL;?>user">Users</a>
                <?php endif; ?>
            <?php else: ?>
                <a href="<?php echo URL;?>login">Login</a>
            <?php endif; ?>
        </div>
